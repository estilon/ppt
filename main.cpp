#include <gecode/int.hh>
#include <gecode/set.hh>
#include <gecode/gist.hh>
#include <gecode/minimodel.hh>
#include <fstream>
#include <map>
#include <dirent.h>
#include <sys/stat.h>

using namespace Gecode;
using namespace std;

namespace{

    int tamanhoDaJornada;
    int quantidadeDeSemanasDaEscala;
    int quantidadeDeMotoristasDaEscala;
    vector<int> ultimaFolga;
    vector<int> ultimaFolgaDomingo;
    vector<tm> horarioFinalDaJornadaDoDiaAnterior;
    int quantidadeDeTabelasDeDiaUtil;
    int quantidadeDeTabelasDeSabado;
    int quantidadeDeTabelasDeDomingo;
    int quantidadeTotalDeTabelas;
    vector<int> quantidadeDeHorariosDaTabela;
    int quantidadeDeTarefas = 0;
    int quantidadeDeTarefasEmDiasUteis = 0;
    int quantidadeDeTarefasEmSabado = 0;
    int quantidadeDeTarefasEmDomingo = 0;

    int limiteInferior = 0;
    int limiteSuperior = quantidadeDeMotoristasDaEscala;

    struct Tarefa{
        int tipoDoDia;
        int indiceDoDiaDaEscala;
        int codigoDaTabela;
        int codifoDoTipoDeHoraio;
        int codigoDoPontoDePartida;
        time_t horaDeInicio;
        time_t horaDeFim;
        tm inicio;
        tm fim;
        int tamanhoDaTarefa;
    };

    vector<Tarefa> tarefas;
    vector< vector<int> > tarefasPorDia;
    vector< vector<int> > tarefasPorTabela;

    string root_directory;
    int timeout;

}

class Tripulacao : public IntMinimizeSpace{

public:

    IntVarArray escala;
    SetVarArray motoristas;
    IntVarArray horasExtras;
    IntVarArray folgaNasHoras;
    IntVar c_cost;

    Tripulacao() : escala(*this, quantidadeDeTarefas, 0 , quantidadeDeMotoristasDaEscala-1),
                   motoristas(*this, quantidadeDeSemanasDaEscala * 7 * quantidadeDeMotoristasDaEscala,
                              IntSet::empty, IntSet(0, quantidadeDeTarefas- 1)),
                   horasExtras(*this, quantidadeDeSemanasDaEscala * 7 * quantidadeDeMotoristasDaEscala, 0, 60*60),
                   folgaNasHoras(*this, quantidadeDeSemanasDaEscala * 7 * quantidadeDeMotoristasDaEscala, 0, 60*60){

        //simetriaDeVariaveis(*this);

        //umaTabelaPorDia(*this);

        diasMaximoDeTrabalhoConsecultivo(*this);

        descancoEntreJornadas(*this);

        horasMaximasConsecultivas(*this);

        folgasNoDomingo(*this);

        calcularLimites();

        time_t timev1;

        time(&timev1);

        Rnd r(timev1);

        c_cost = expr(*this, cardinality(channel(*this,escala)));

        //rel(*this, c_cost, IRT_LQ, limiteSuperior);
        rel(*this, c_cost, IRT_GQ, limiteInferior);

        IntAFC afc(*this, escala);

        IntActivity activity(*this, escala);

        branch(*this, escala, INT_VAR_NONE(), INT_VAL_MED());
        //branch(*this, motoristas, SET_VAR_NONE(), SET_VAL_MIN_INC());


    }


    Tripulacao(bool share, Tripulacao& s) : IntMinimizeSpace(share, s){
        escala.update(*this, share, s.escala);
        c_cost.update(*this, share, s.c_cost);
        motoristas.update(*this, share, s.motoristas);
        horasExtras.update(*this, share, s.horasExtras);
        folgaNasHoras.update(*this, share, s.folgaNasHoras);
    }

    virtual Space* copy(bool share){
        return new Tripulacao(share, *this);
    }

    void print(std::ostream& os) const {
        os << escala[0];
        for(int i = 1; i < quantidadeDeTarefas; i++){
            os << " " << escala[i];
        }
    }

    void print_matrix(std::ostream& os) const{

        if(this->failed()){
            os << "Falhou";
            return;
        }

        int matrix[quantidadeDeMotoristasDaEscala][quantidadeDeTarefas];

        memset(matrix, 0, sizeof matrix);

        /*for(int i = 0; i < quantidadeDeTarefas; i++){
            matrix[escala[i].val()][i] = 1;
        }

        for(int i = 0; i < quantidadeDeMotoristasDaEscala; i++){
            for(int j = 0; j < quantidadeDeTarefas; j++){
               // os << matrix[i][j] << " ";
            }
           // os << endl;
        }*/
        os << c_cost << endl;
    }

    void print(void) const{

        for(int i = 0; i < quantidadeDeSemanasDaEscala * 7; i++){
            for(int j = 0; j < tarefasPorDia[i].size(); j ++) {
                cout << escala[tarefasPorDia[i][j]] << " Incio: ";
                cout << tarefas[tarefasPorDia[i][j]].inicio.tm_hour << ":" <<
                tarefas[tarefasPorDia[i][j]].inicio.tm_min;
                cout << " Fim: " << tarefas[tarefasPorDia[i][j]].fim.tm_hour << ":" <<
                tarefas[tarefasPorDia[i][j]].fim.tm_min;
                cout << endl;
            }
            cout << endl;
        }
        //cout << motoristas << endl;
        //cout << horasExtras << endl;
        //cout << folgaNasHoras << endl;
        cout << "Custo " << c_cost << endl;
    }

    void print_normal(void) const{

        for(int i = 0; i < quantidadeDeSemanasDaEscala * 7; i++){
            for(int j = 0; j < tarefasPorDia[i].size(); j ++) {
                cout << escala[tarefasPorDia[i][j]] << " ";
            }
            cout << endl;
        }
        //cout << motoristas << endl;
        //cout << horasExtras << endl;
        //cout << folgaNasHoras << endl;
        cout << "Custo " << c_cost << endl;
    }

    virtual IntVar cost(void) const {
        return c_cost;
    }

    virtual void constrain(const Space& _b){
        const Tripulacao& b = static_cast<const Tripulacao&>(_b);
        rel(*this, c_cost, IRT_LE, b.c_cost.val());
    }


private:

    void calcularLimites(){

        int somaDasHoras = 0;
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < tarefasPorDia[i].size(); j++){
                somaDasHoras += tarefas[tarefasPorDia[i][j]].tamanhoDaTarefa;
            }
        }

        limiteInferior = somaDasHoras/(7*(7*60 + 20 + 120));

        limiteSuperior = limiteInferior * 2.3;

    }

    void simetriaDeVariaveis(Space& home) const {
        for(int i = 0; i < quantidadeDeSemanasDaEscala * 7; i++) {
            IntVarArgs temp;
            for (int j = 0; j < tarefasPorDia[i].size(); j++) {
                temp << escala[tarefasPorDia[i][j]];
            }
            IntArgs args = IntArgs::create(14, 0);
            //eprecede(home, temp, args);
        }

    }

    void tarefasPorDiaAux(Space &home, int i, int quantidadeDeTabelas, int inicioDasTabelas) const{
        int ultimaTarefaDaTabela = 0;
        
        for(int j = inicioDasTabelas; j < inicioDasTabelas + quantidadeDeTabelas; j++){
            int primeiraTarefaDaTabela = ultimaTarefaDaTabela;
            ultimaTarefaDaTabela = primeiraTarefaDaTabela + quantidadeDeHorariosDaTabela[j];

            for(int k = primeiraTarefaDaTabela; k < ultimaTarefaDaTabela; k++){
                for(int l = ultimaTarefaDaTabela; l < tarefasPorDia[i].size(); l++){
                    int tarefa1 = tarefasPorDia[i][k];
                    int tarefa2 = tarefasPorDia[i][l];
                    rel(home, escala[tarefa1], IRT_NQ, escala[tarefa2]);
                }
            }
        }
    }

    void umaTabelaPorDia(Space &home) const {
        for(int i = 0; i < (quantidadeDeSemanasDaEscala*7); i++){
            if(i % 7 != 5 && i % 7 != 6){
                tarefasPorDiaAux(home, i, quantidadeDeTabelasDeDiaUtil, 0);

            }else if(i % 7 == 5){
                tarefasPorDiaAux(home, i, quantidadeDeTabelasDeSabado, quantidadeDeTabelasDeDiaUtil);


            }else if(i % 7 == 6){
                tarefasPorDiaAux(home, i, quantidadeDeTabelasDeDomingo, quantidadeDeTabelasDeDiaUtil+quantidadeDeTabelasDeSabado);
            }
        }
    }

    void diasMaximoDeTrabalhoConsecultivo(Space& home) const {

        Matrix<SetVarArray> tarefasDoMotorista(motoristas, quantidadeDeMotoristasDaEscala, quantidadeDeSemanasDaEscala * 7 );

        int qtdDias = quantidadeDeSemanasDaEscala * 7;


        for(int i = 0; i <= (qtdDias - 7); i ++){

            for(int j = 0; j < quantidadeDeMotoristasDaEscala; j++){

                IntVar count(home, 0, 0);

                for(int k = i; k < i + 7; k++){
                    count = expr(home, ite(cardinality(tarefasDoMotorista(j, k)) == 0, count + 1, count + 0));
                }
                rel(home, count >= 1, ICL_DOM);
            }
        }

        for(int i = 0; i < quantidadeDeMotoristasDaEscala; i++){

            int qtdDiasDeTrabalho = 7 - ultimaFolga[i];
            IntVar count(home, 0, 0);
            for(int j = 0; j < qtdDiasDeTrabalho; j++){
                count = expr(home, ite(cardinality(tarefasDoMotorista(i, j)) == 0, count + 1, count + 0), ICL_DOM);
            }
            rel(home, count >= 1, ICL_DOM);
        }
    }

    void horasMaximasConsecultivas(Space& home) const {

        Matrix<SetVarArray> tarefasDoMotorista(motoristas, quantidadeDeMotoristasDaEscala, quantidadeDeSemanasDaEscala * 7 );
        Matrix<IntVarArray> horasExtrasDoMotorista(horasExtras, quantidadeDeMotoristasDaEscala, quantidadeDeSemanasDaEscala * 7);
        Matrix<IntVarArray> folgaNasHorasDoMotorista(folgaNasHoras, quantidadeDeMotoristasDaEscala, quantidadeDeSemanasDaEscala * 7);
        int tamanhoMaximoDeJornada = 7*60 + 20;
        for(int i = 0, c = 0; i < quantidadeDeSemanasDaEscala * 7; i++, c++){

            unsigned  int t = tarefasPorDia[i].size();
            IntVarArgs tarefasDoDia(t);
            IntArgs duracao(t);
            IntArgs indiceDasTarefas(t);

            for(int j = 0; j < tarefasPorDia[i].size(); j++){

                tarefasDoDia[j] = escala[tarefasPorDia[i][j]];
                duracao[j] = tarefas[tarefasPorDia[i][j]].tamanhoDaTarefa;
                indiceDasTarefas[j] = j;
            }


            channel(home, tarefasDoDia, tarefasDoMotorista.row(i));

            for(int j = 0; j < quantidadeDeMotoristasDaEscala; j++){
                IntVar limite(home, 0, 590);
                rel(home, limite, IRT_LQ, tamanhoMaximoDeJornada + 120);
                //rel(home, limite, IRT_GQ, expr(home, ite(cardinality(tarefasDoMotorista(j,i)) == 0, 0, 4 * 60)));
                weights(home, indiceDasTarefas, duracao, tarefasDoMotorista(j,i), limite);
                horasExtrasDoMotorista(j, i) = expr(home, ite(limite - (tamanhoMaximoDeJornada) > 0, (limite - tamanhoMaximoDeJornada), 0));
                folgaNasHorasDoMotorista(j, i) = expr(home, ite(limite - tamanhoMaximoDeJornada > 0, 0, -(limite - tamanhoMaximoDeJornada)));
                //rel(home, limite >= expr(home, ite(folgaNasHorasDoMotorista(j, i) == tamanhoDaJornada, 0, 120)));

            }
        }



        for(int j = 0; j <= (quantidadeDeSemanasDaEscala * 7) - 7; j += 7){

            IntVarArgs temp;
            //IntVarArgs folga;

            for(int i = 0; i < quantidadeDeMotoristasDaEscala; i++) {

                for (int k = j; k < j + 7; k++) {
                    temp << horasExtrasDoMotorista(i, k);
                    //folga << folgaNasHorasDoMotorista(i, k);
                }
            }
            rel(home, sum(temp) <= 50 * 60, ICL_DOM);
            //rel(home, sum(folga) <= 44 * 60, ICL_DOM);
        }
    }

    void descancoEntreJornadas(Space &home) const {
        //Resrtricao 9
        for(int i = 0; i < quantidadeDeMotoristasDaEscala; i++){

            for(int j = 0; j < tarefasPorDia[0].size(); j++){
                int t = tarefasPorDia[0][j];

                int fimDaJornadaEmMiniutos = horarioFinalDaJornadaDoDiaAnterior[i].tm_hour * 60 + horarioFinalDaJornadaDoDiaAnterior[i].tm_min;
                int proximaJornada = tarefas[t].inicio.tm_hour*60 + tarefas[t].inicio.tm_min;
                if(horarioFinalDaJornadaDoDiaAnterior[i].tm_hour > 13){
                    int descanso = (1440 - fimDaJornadaEmMiniutos) + (proximaJornada);

                    if(descanso < 11*60){
                        rel(home, escala[t], IRT_NQ, i, ICL_DOM);
                    }
                }
            }
        }
        //Restricao 8
        //0-1-2-4-404-378-01/04/2013 05:24-01/04/2013 16:51
        int qtdDias = quantidadeDeSemanasDaEscala * 7;
        for(int i = 0; i < qtdDias - 1; i++){
            int qtdTarefasDoDia = tarefasPorDia[i].size();
            for(int j = 0; j < qtdTarefasDoDia; j++){
                int qtdTarefasDoProximoDia = tarefasPorDia[i+1].size();
                for(int k = 0; k < qtdTarefasDoProximoDia; k++){
                    int tarefa1 = tarefasPorDia[i][j];
                    int tarefa2 = tarefasPorDia[i+1][k];

                    int fimDaJornada = tarefas[tarefa1].fim.tm_hour * 60 + tarefas[tarefa1].fim.tm_min;
                    int proximaJornada = tarefas[tarefa2].inicio.tm_hour *60 + tarefas[tarefa2].inicio.tm_min;

                    int descanso = (1440 - fimDaJornada) + proximaJornada;

                    if(descanso < 11 * 60){
                        rel(home, escala[tarefa1], IRT_NQ, escala[tarefa2], ICL_DOM);
                    }
                }
            }
        }
    }

    void folgasNoDomingo(Space &home) const {

        Matrix<SetVarArray> tarefasDoMotorista(motoristas, quantidadeDeMotoristasDaEscala, quantidadeDeSemanasDaEscala * 7 );

        int qtdDias = quantidadeDeSemanasDaEscala * 7;

        for(int i = 0; i < quantidadeDeMotoristasDaEscala; i++){

            int qtdDomingosDeTrabalho = 7 - ultimaFolga[i];

            if(qtdDomingosDeTrabalho > quantidadeDeSemanasDaEscala) continue;

            IntVar count(home, 0, 0);

            int k = 0;
            for(int j = 6; j < qtdDomingosDeTrabalho*7; j+=7){
                count = expr(home, ite(cardinality(tarefasDoMotorista(i, j)) == 0, count + 1, count + 0), ICL_DOM);
            }

            rel(home, count >= 1, ICL_DOM);
        }

        if(quantidadeDeSemanasDaEscala < 6) return;

        for(int i = 6; i < qtdDias; i += 7){

            IntVar count(home, 0, 0);

            for(int j = 0; j < quantidadeDeMotoristasDaEscala; j++){
                for(int k = i; k < i + 7*7 && k < qtdDias; k += 7){
                    count = expr(home, ite(cardinality(tarefasDoMotorista(k, j)) == 0, count + 1, count + 0), ICL_DOM);
                }

                rel(home, count >= 1, ICL_DOM);
            }
        }

    }
};

void initialize();

void solveModel(string root_directory, string fileName) {
    Tripulacao* m = new Tripulacao;

    /*Gist::Print<Tripulacao> p("Print solution");
    Gist::Options o;
    o.inspect.click(&p);
    o.threads = -2;
    Gist::dfs(m,o);*/



    clock_t tempo;

    Search::Cutoff* cutoff = Search::Cutoff::rnd(tempo, 100, 10000, 20);

    Search::Options options = Search::Options();
    Search::Stop* time_out = Search::Stop::time(timeout);
    options.stop = time_out;
    options.threads = -1;
    options.cutoff = cutoff;

    RBS<BAB, Tripulacao> rbs(m, options);


    //BAB<Tripulacao> e(m, options);

    ofstream answer;
    string pathToAnswear = root_directory + "-respostas";
    mkdir(pathToAnswear.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);

    pathToAnswear += "/" + fileName;

    answer.open (pathToAnswear, fstream::app);

    clog << "Solving to " + fileName << endl;

    tempo = clock();

    while(Tripulacao *t = rbs.next()){
        answer << "VAR NONE VAL MED" << endl;
        cout << "VAR NONE VAL MED" << endl;
        t->print_matrix(answer);
        //t->print_normal();
        //cout << rbs.statistics().wmp;
        //cout << t->failed() << "Falhou";
        //cout << t->failed() << "Falhou";


        delete t;
    }
    tempo = clock() - tempo;
    answer  << tempo/(double) CLOCKS_PER_SEC << endl;

    if(answer.fail()){
        clog << "problemas ao abrir o arquivo " + pathToAnswear << endl;
    }

    answer.close();
    cout << "Tempo : " << tempo/(double) CLOCKS_PER_SEC << endl;
    clog << "Tempo : " << tempo/(double) CLOCKS_PER_SEC << endl;

    delete m;
}


void lerEntradas(string fileName) {
    cout << fileName << endl;
    initialize();

    FILE* entrada =  fopen(fileName.c_str(),"r");

    if(entrada == NULL) {
        clog << "Arquivo não existe " << fileName << endl;
        return;
    }

    fscanf(entrada, "%d", &tamanhoDaJornada);
    fscanf(entrada, "%d", &quantidadeDeSemanasDaEscala);
    fscanf(entrada, "%d", &quantidadeDeMotoristasDaEscala);
    //tamanhoDaJornada = 7 * 60 + 20;
    int temp;
    for(int i = 0; i < quantidadeDeMotoristasDaEscala; i++){
        fscanf(entrada, "%d", &temp);
        ultimaFolga.push_back(temp);
    }

    for(int i = 0; i < quantidadeDeMotoristasDaEscala; i++){
        fscanf(entrada, "%d", &temp);
        ultimaFolgaDomingo.push_back(temp);
    }

    for(int i = 0; i < quantidadeDeMotoristasDaEscala; i++){
        char temp[] = "07:00";
        fscanf(entrada, "%s", temp);

        tm horarioUltimaJornada;

        strptime(temp, "%H:%M", &horarioUltimaJornada);

        horarioUltimaJornada.tm_year = 1;
        horarioUltimaJornada.tm_mon = 1;
        horarioUltimaJornada.tm_mday = 1;
        horarioUltimaJornada.tm_sec = 0;
        horarioUltimaJornada.tm_zone = "BRT";

        horarioFinalDaJornadaDoDiaAnterior.push_back(horarioUltimaJornada);
    }

    fscanf(entrada, "%d", &quantidadeDeTabelasDeDiaUtil);
    fscanf(entrada, "%d", &quantidadeDeTabelasDeSabado);
    fscanf(entrada, "%d", &quantidadeDeTabelasDeDomingo);
    fscanf(entrada, "%d", &quantidadeTotalDeTabelas);

    int a, b;

    for(int i = 0; i < quantidadeDeTabelasDeDiaUtil; i++){
        fscanf(entrada, "%d-%d-%d", &a, &b, &temp);
        quantidadeDeHorariosDaTabela.push_back(temp);
        quantidadeDeTarefasEmDiasUteis += temp*quantidadeDeSemanasDaEscala * 5;
    }

    for(int i = 0; i < quantidadeDeTabelasDeSabado; i++){
        fscanf(entrada, "%d-%d-%d", &a, &b, &temp);
        quantidadeDeHorariosDaTabela.push_back(temp);
        quantidadeDeTarefasEmSabado += temp*quantidadeDeSemanasDaEscala;
    }

    for(int i = 0; i < quantidadeDeTabelasDeDomingo; i++){
        fscanf(entrada, "%d-%d-%d", &a, &b, &temp);
        quantidadeDeHorariosDaTabela.push_back(temp);
        quantidadeDeTarefasEmDomingo += temp*quantidadeDeSemanasDaEscala;
    }

    quantidadeDeTarefas = quantidadeDeTarefasEmDomingo + quantidadeDeTarefasEmSabado + quantidadeDeTarefasEmDiasUteis;

    for(int i = 0; i < quantidadeDeTarefas; i++){
        Tarefa tarefa;
        char inicio[17];
        char fim[17];

        fscanf(entrada, " %d-%d-%d-%d-%d-%d-%[^-]-%[^\n\r]",
               &tarefa.tipoDoDia,
               &tarefa.indiceDoDiaDaEscala,
               &tarefa.codigoDaTabela,
               &tarefa.codifoDoTipoDeHoraio,
               &tarefa.tamanhoDaTarefa,
               &tarefa.codigoDoPontoDePartida,
               inicio,
               fim);

        tm inicioTemp;
        tm fimTemp;

        strptime(inicio,"%d/%m/%Y %H:%M", &inicioTemp);
        strptime(fim,"%d/%m/%Y %H:%M", &fimTemp);

        inicioTemp.tm_sec = 0;
        fimTemp.tm_sec = 0;

        tarefa.horaDeInicio = mktime(&inicioTemp);
        tarefa.horaDeFim = mktime(&fimTemp);

        inicioTemp.tm_year = 0;
        inicioTemp.tm_mon = 0;
        inicioTemp.tm_mday = 0;

        tarefa.fim = fimTemp;
        tarefa.inicio = inicioTemp;

        tarefas.push_back(tarefa);
    }
}

void relacionarTarefasComDiasETabelas(){

    for(int i = 0; i < quantidadeDeSemanasDaEscala * 7; i++){
        tarefasPorDia.push_back(*(new vector<int>));
    }

    for(int i = 0; i < quantidadeTotalDeTabelas; i++){
        tarefasPorTabela.push_back(*(new vector<int>));
    }

    for(int i = 0; i < quantidadeDeTarefas; i++){

        auto tarefa = tarefas[i];

        tarefasPorDia[tarefa.indiceDoDiaDaEscala-1].push_back(i);
        tarefasPorTabela[tarefa.codigoDaTabela-1].push_back(i);
    }
}

void initialize(){
    tamanhoDaJornada = 0;
    quantidadeDeSemanasDaEscala = 0;
    quantidadeDeMotoristasDaEscala = 0;
    ultimaFolga = vector<int>();
    ultimaFolgaDomingo = vector<int>();
    horarioFinalDaJornadaDoDiaAnterior = vector<tm>();
    quantidadeDeTabelasDeDiaUtil = 0;
    quantidadeDeTabelasDeSabado = 0;
    quantidadeDeTabelasDeDomingo = 0;
    quantidadeTotalDeTabelas = 0;
    quantidadeDeHorariosDaTabela = vector<int>();
    quantidadeDeTarefas = 0;
    quantidadeDeTarefasEmDiasUteis = 0;
    quantidadeDeTarefasEmSabado = 0;
    quantidadeDeTarefasEmDomingo = 0;

    limiteInferior = 0;
    limiteSuperior = quantidadeDeMotoristasDaEscala;

    tarefas = vector<Tarefa>();
    tarefasPorDia = vector< vector<int> >();
    tarefasPorTabela = vector< vector<int> >();
}

int main(int argsc, char* argsv[]){

    root_directory = argsv[1];
    string fileName = argsv[2];
    timeout = atoi(argsv[3]);

    lerEntradas(root_directory+"/"+fileName);
    relacionarTarefasComDiasETabelas();
    solveModel(root_directory,fileName);

    return 0;

}
