#include <dirent.h>
#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <fstream>

//
// Created by estilon on 10/11/16.
//

using namespace std;

ofstream log;

void all_files(const char* root, int level){
    DIR* dir = opendir(root);

    if(dir == NULL) return;

    log << "Entering " << root << " directory" << endl;

    dirent* next = readdir(dir);

    while(next != NULL){

        if(next->d_type == DT_DIR && strcmp(next->d_name, ".") != 0  && strcmp(next->d_name, "..") != 0){
            string name_of_the_dirctory = next->d_name;
            size_t found = name_of_the_dirctory.find("respostas");
            if(found != string::npos) {
                next = readdir(dir);
                continue;
            }
            char path[strlen(root) + strlen(next->d_name)];
            strcpy(path, root);
            strcat(path, "/");
            strcat(path, next->d_name);
            all_files(path, level + 1);
        }else if(strcmp(next->d_name, ".") != 0  && strcmp(next->d_name, "..") != 0){
            char path[strlen(root) + strlen(next->d_name)];
            string old_path;
            strcpy(path, root);
            old_path = path;
            strcat(path, "/");
            strcat(path, next->d_name);

            string command_create_directory = "./PPT " + old_path + " " + next->d_name + " " + "180000 >> saida.txt";

            log << command_create_directory << endl;

            system(command_create_directory.c_str());
        }

        next = readdir(dir);
    }
}

int main(int argsc, char* argsv[]){

    log.open("log.txt", ofstream::app);

    string root_directory = argsv[1];

    int timeout = atoi(argsv[2]);

    all_files(root_directory.c_str(), 1);

    return 0;

}
